#!/bin/bash

runTest() {
    echo "Running $1"
    make tests/threads/$1.result TIMEOUT=10
}

cd threads/build

echo "Testing thread sleep mechanism i.e. avoid busy_waiting"
runTest alarm-single
runTest alarm-zero
runTest alarm-negative
runTest alarm-simultaneous

echo "Testing priority scheduling implementation"
runTest alarm-priority
runTest priority-change
runTest priority-fifo

echo "Testing thread synchronization using semaphores"
runTest priority-sema
