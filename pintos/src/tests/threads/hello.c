/* Creates N threads, each of which sleeps a different, fixed
   duration, M times.  Records the wake-up order and verifies
   that it is valid. */

#include <stdio.h>
#include "tests/threads/tests.h"

void
test_hello (void)
{
  printf("Hello.... ");
  thread_print_stats();
  msg("hello from msg.");
}
